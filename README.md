> One themes for use with react-ace
### Install

```
npm install --save ace-theme-mcode
```

### Use

```JS
import 'ace-theme-mcode/'


### Done
That's it. Big props to the creators of the color schemes of the one themes!


### v1.0.0
初始化白色主题
### v1.1.0
调整代码列数字 padding 间距、字体颜色、背景颜色
### v1.2.0
调整代码编辑区光标不对称问题
### v1.3.0
修复光标间距问题
### v1.4.0
新增暗黑模式
### v1.5.0
新增 ts 声明文件
### v1.6.0
移除依赖
### v1.7.0
修复暗黑模式字体颜色、行距
### v1.8.0
修复暗黑模式背景、字体颜色
### v1.9.0
修复日间模式行数栏背景和边框颜色
### v1.10.0
修复日间模式、日间模式行距
