ace.define("ace/theme/mcode_night",["require","exports","module","ace/lib/dom"], function(acequire, exports, module) {

  exports.isDark = true;
  exports.cssClass = "ace-mcode_night";
  exports.cssText = ".ace-mcode_night .ace_gutter {\
  background: #181A26;\
  color: #1F8FC6\
  }\
  .ace-mcode_night .ace_print-margin {\
  width: 1px;\
  background: #555651\
  }\
  .ace-mcode_night {\
  background-color: #20232F;\
  color: #F8F8F2;\
  }\
  .ace-mcode_night .ace_cursor {\
  color: #F8F8F0\
  }\
  .ace-mcode_night .ace_marker-layer .ace_selection {\
  background: #49483E\
  }\
  .ace-mcode_night.ace_multiselect .ace_selection.ace_start {\
  box-shadow: 0 0 3px 0px #272822;\
  }\
  .ace-mcode_night .ace_marker-layer .ace_step {\
  background: rgb(102, 82, 0)\
  }\
  .ace-mcode_night .ace_marker-layer .ace_bracket {\
  margin: -1px 0 0 -1px;\
  border: 1px solid #49483E\
  }\
  .ace-mcode_night .ace_marker-layer .ace_active-line {\
  background: #2B303A\
  }\
  .ace-mcode_night .ace_gutter-active-line {\
  background-color: inherit\
  }\
  .ace-mcode_night .ace_marker-layer .ace_selected-word {\
  border: 1px solid #49483E\
  }\
  .ace-mcode_night .ace_invisible {\
  color: #52524d\
  }\
  .ace-mcode_night .ace_entity.ace_name.ace_tag,\
  .ace-mcode_night .ace_keyword,\
  .ace-mcode_night .ace_meta.ace_tag,\
  .ace-mcode_night .ace_storage {\
  color: #F92672\
  }\
  .ace-mcode_night .ace_punctuation,\
  .ace-mcode_night .ace_punctuation.ace_tag {\
  color: #fff\
  }\
  .ace-mcode_night .ace_constant.ace_character,\
  .ace-mcode_night .ace_constant.ace_language,\
  .ace-mcode_night .ace_constant.ace_numeric,\
  .ace-mcode_night .ace_constant.ace_other {\
  color: #AE81FF\
  }\
  .ace-mcode_night .ace_invalid {\
  color: #F8F8F0;\
  background-color: #F92672\
  }\
  .ace-mcode_night .ace_invalid.ace_deprecated {\
  color: #F8F8F0;\
  background-color: #AE81FF\
  }\
  .ace-mcode_night .ace_support.ace_constant,\
  .ace-mcode_night .ace_support.ace_function {\
  color: #66D9EF\
  }\
  .ace-mcode_night .ace_fold {\
  background-color: #A6E22E;\
  border-color: #F8F8F2\
  }\
  .ace-mcode_night .ace_storage.ace_type,\
  .ace-mcode_night .ace_support.ace_class,\
  .ace-mcode_night .ace_support.ace_type {\
  font-style: italic;\
  color: #66D9EF\
  }\
  .ace-mcode_night .ace_entity.ace_name.ace_function,\
  .ace-mcode_night .ace_entity.ace_other,\
  .ace-mcode_night .ace_entity.ace_other.ace_attribute-name,\
  .ace-mcode_night .ace_variable {\
  color: #A6E22E\
  }\
  .ace-mcode_night .ace_variable.ace_parameter {\
  font-style: italic;\
  color: #FD971F\
  }\
  .ace-mcode_night .ace_string {\
  color: #E6DB74\
  }\
  .ace-mcode_night .ace_comment {\
  color: #75715E\
  }\
  .ace-mcode_night .ace_indent-guide {\
  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAEklEQVQImWPQ0FD0ZXBzd/wPAAjVAoxeSgNeAAAAAElFTkSuQmCC) right repeat-y\
  }";
  
  var dom = acequire("../lib/dom");
  dom.importCssString(exports.cssText, exports.cssClass);
  });
  